package be.quodlibet.boxable;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import java.io.IOException;

/**
 * Created by dgautier on 3/18/2015.
 */
public class BaseTable extends Table<PDPage> {
	
	public static enum LINES {
		DRAW, DRAW_NOTHING;
	}

	public static enum CONTENT {
		DRAW, DRAW_NOTHING;
	}
	
	
    public BaseTable(float yStart, float yStartNewPage, float bottomMargin, float width, float margin, PDDocument document, PDPage currentPage,LINES drawLine, CONTENT drawContent) throws IOException {
        super(yStart, yStartNewPage, bottomMargin, width, margin, document, currentPage, drawLine, drawContent);
    }

    @Override
    protected void loadFonts() {
        // Do nothing as we don't have any fonts to load
    }

    @Override
    protected PDPage createPage() {
        PDPage page =  new PDPage();
        this.getDocument().addPage(page);
        return page;
    }
}
