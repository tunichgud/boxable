package be.quodlibet.boxable;

import org.apache.pdfbox.pdmodel.PDDocument;

import be.quodlibet.boxable.BaseTable.CONTENT;
import be.quodlibet.boxable.BaseTable.LINES;

import java.io.IOException;

/**
 * Created by dgautier on 3/18/2015.
 */
public abstract class AbstractTemplatedTable<T extends AbstractPageTemplate> extends Table<T> {

    public AbstractTemplatedTable(float yStart, float yStartNewPage, float bottomMargin, float width, float margin, PDDocument document, T currentPage,LINES drawLine, CONTENT drawContent) throws IOException {
        super(yStart, yStartNewPage, bottomMargin, width, margin, document, currentPage, drawLine, drawContent);
    }

    public AbstractTemplatedTable(float yStartNewPage, float bottomMargin, float width, float margin, PDDocument document,LINES drawLine, CONTENT drawContent) throws IOException {
        super(yStartNewPage, bottomMargin, width, margin, document, drawLine, drawContent);
        setYStart(getCurrentPage().yStart());
    }

}
